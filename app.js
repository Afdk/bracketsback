const Express = require("express");
const BodyParser = require("body-parser");

var user_routes = require('./routes/user')
var mongo = require('./services/mongo');
var app = Express();

app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));
app.use('/api', user_routes);

app.listen(5000, () => {
  mongo.startDBManager();
});
/*
app.post("/users", (request, response) => {
    collection.insertOne(request.body, (error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
        response.send(result.result);
    });
});
*/
app.get("/users", (request, response) => {
  collection.find({}).toArray((error, result) => {
      if(error) {
          return response.status(500).send(error);
      }
      console.log(result)
      response.send(result);
  });
});

app.get("/user/:id", (request, response) => {
    collection.findOne({ "_id": new ObjectId(request.params.id) }, (error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
        response.send(result);
    });
});