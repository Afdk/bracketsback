'use strict'
var express = require('express');
var UserController = require('../controller/user');

var api = express.Router();

api.get('/getUser', UserController.getUser);
api.post('/newUser', UserController.newUser);

module.exports = api;