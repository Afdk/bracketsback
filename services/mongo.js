const MongoClient = require("mongodb").MongoClient;
const ObjectId = require("mongodb").ObjectID;
const CONNECTION_URL = 'mongodb+srv://admin:admin123@brackets-otsny.mongodb.net/test?retryWrites=true&w=majority';
const DATABASE_NAME = "Brackets";

var database, collection;
function startDBManager() {
  MongoClient.connect(CONNECTION_URL, { useNewUrlParser: true }, (error, client) => {
    if(error) {
        throw error;
    }
    database = client.db(DATABASE_NAME);
    collection = database.collection("Users");
    console.log("Connected to '" + DATABASE_NAME + "'!");
  });
}

function getDB(){
  return database
}
async function InsertOne(newItem) {
  let idk = await collection.insertOne(newItem, (error, result) => {
    if(error) {
        return error;
    }
    return result;
  })
  console.log('idk')
  console.log(idk)
  return idk
}
module.exports = { startDBManager, InsertOne, MongoClient, database, getDB };