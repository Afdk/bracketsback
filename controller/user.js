'use strict'

var { MongoClient, getDB} = require('../services/mongo');


function getUser(req, res){
  res.status(200).send({
    message: 'holis'
  })
}


async function newUser (request, response){
  var database = getDB()
  var collection = database.collection("Users");
  collection.insertOne(request.body, (error, result) => {
    if(error) {
        return response.status(500).send(error);
    }
    response.send(result.result);
  });
};

module.exports = {
  getUser,
  newUser
};